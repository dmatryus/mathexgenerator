import datetime
import string
from functools import reduce

import numpy as np
import pylatex
from numpy.random import randint


class P:
    def __init__(self, max_num=9, n_num=3, max_lit=3, max_op=5):
        self.n_num = n_num
        self.max_num = max_num
        self.max_lit = max_lit if max_lit <= len(string.ascii_lowercase) else len(string.ascii_lowercase)

        self.max_op = max_op
        self.ops = randint(0, 4, max_op)


class Ex:
    lit_dict = {i: j for i, j in enumerate(string.ascii_lowercase)}
    op_dict = {0: '+', 1: '-', 2: '*', 3: ':'}

    def __init__(self, p=None, num_complexity=3, lit_complexity=3):
        self.p = P() if p is None else p
        self.num_complexity = num_complexity
        self.nums = np.append(randint(0, self.p.max_num, self.p.n_num), [0])
        self.lit_complexity = lit_complexity

        self.ops = p.ops
        self.size = len(self.ops) + 1
        self.top_nums = np.zeros(self.size)
        self.bot_nums = np.zeros(self.size)

        self.top_lits = []
        self.bot_lits = []

        for i in range(self.size):
            self.top_nums[i] = reduce(lambda x, y: x * self.nums[y],
                                      randint(0, self.p.n_num, self.num_complexity + 1))
            self.bot_nums[i] = reduce(lambda x, y: x * self.nums[y],
                                      randint(0, self.p.n_num, self.num_complexity + 1))
            self.top_lits.append({i: randint(0, self.lit_complexity) for i in range(self.p.max_lit)})
            self.bot_lits.append({i: randint(0, self.lit_complexity) for i in range(self.p.max_lit)})

    def __lit_concatenation(self, location='both'):
        r = []
        ls = None
        if location == 'both':
            return self.__lit_concatenation('top'), self.__lit_concatenation('bot')
        else:
            if location == 'top':
                ls = self.top_lits
            elif location == 'bot':
                ls = self.bot_lits
            for i in ls:
                tr = ''
                for j in i.items():
                    if j[1] > 0:
                        if j[1] == 1:
                            tr += self.lit_dict[j[0]]
                        else:
                            tr += '{}^{}'.format(self.lit_dict[j[0]], j[1])
                r.append(tr)
            return r

    def __str__(self):
        s = ''
        lc_top, lc_bot = self.__lit_concatenation()
        for i in range(self.size):
            top = '{} {}'.format(self.top_nums[i], lc_top[i])
            bot = '{} {}'.format(self.bot_nums[i], lc_bot[i])
            s += '{} / {}'.format(top, bot)
            if i < self.size - 1:
                s += ' {}\n'.format(self.op_dict[self.ops[i]])
        return s

    def get_latex_math(self):
        data = ''
        lc_top, lc_bot = self.__lit_concatenation()
        for i in range(self.size):
            if self.top_nums[i] * self.bot_nums[i] != 0:
                if i > 0 and data != '':
                    data += ' {} '.format(self.op_dict[self.ops[i - 1]])
                data += '\\frac{{{0}{1}}}{{{2}{3}}}'.format(int(self.top_nums[i]), lc_top[i], int(self.bot_nums[i]),
                                                            lc_bot[i])
        if data == '':
            agn = Ex(self.p, self.num_complexity, self.lit_complexity).get_latex_math()
        else:
            agn = pylatex.Alignat(numbering=False, escape=False)
            agn.append(data)
        return agn


def generate_exs(num, p=None, num_complexity=3, lit_complexity=3):
    return [Ex(p, num_complexity, lit_complexity).get_latex_math() for i in range(num)]


def get_pdf(exs, document_title=None, file_path=None):
    doc = pylatex.Document()
    if document_title is None:
        document_title = str(datetime.date.today())
    with doc.create(pylatex.Section(document_title, numbering=False)):
        with doc.create(pylatex.Enumerate()) as enum:
            for i in exs:
                enum.add_item(i)

    doc.generate_pdf(file_path, clean_tex=True)
